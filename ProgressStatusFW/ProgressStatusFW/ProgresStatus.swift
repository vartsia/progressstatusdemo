//
//  ProgresStatus.swift
//  ProgressStatusFW
//
//  Created by Juhani Nikumaa on 17/09/2018.
//  Copyright © 2018 Juhani Nikumaa. All rights reserved.
//

import Foundation

/**
 Protocol that defines the functions to get and set progress status.
 Any object that implements this protocol can acces the status.
 */
public protocol ProgressStatus {
    func getStatus() -> Double
    func setStatus(newVal: Double)
}


/**
 Example class that does some work.
 Foreach loop is used to demo the wordload which status needs to be observerd.
 */
public class ClassThatDoesWork {
    
    public init() {}
    
    /**
     Variable status is object that conforms to the ProgressStatus protocol.
     To set it as optional parameter it can be used only when needed.
     */
    public func doWork(howMuch: Int, status: ProgressStatus? = nil) {
        
        for i in 0...howMuch {
            /**
             Because the status is a variable that might be used in UI
             we update the value in the main thread even thou the actual
             work is done in background thread.
             */
            DispatchQueue.main.async {
                status?.setStatus(newVal: Double(i)/Double(howMuch))
            }
        }

    }
}
