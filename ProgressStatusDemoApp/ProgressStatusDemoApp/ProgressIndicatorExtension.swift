//
//  ProgressIndicatorExtension.swift
//  ProgressStatusDemoApp
//
//  Created by Juhani Nikumaa on 17/09/2018.
//  Copyright © 2018 Juhani Nikumaa. All rights reserved.
//

import Cocoa
import ProgressStatusFW

/**
 To take advance of the protocol in our example we update the UI:s
 ProgressIndicator and to do that we create extensions for NSProgressIndicator
 object that sets and gets the status.
 */
extension NSProgressIndicator: ProgressStatus {
    /**
     Because NSProgressIndicator already have a propreties for it's status,
     we only use the protocol as a wrapper to these values.
     */
    public func getStatus() -> Double {
        return self.doubleValue
    }
    
    public func setStatus(newVal: Double) {
        self.doubleValue = newVal
    }
}
