//
//  AppDelegate.swift
//  ProgressStatusDemoApp
//
//  Created by Juhani Nikumaa on 17/09/2018.
//  Copyright © 2018 Juhani Nikumaa. All rights reserved.
//

import Cocoa
import ProgressStatusFW

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    var workingClass = ClassThatDoesWork()
    
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var progressBar: NSProgressIndicator!
    @IBOutlet weak var startButton: NSButton!
    
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        /**
         It's important to set the min and max values correctly since
         the range used by the protocols IMPLEMENTATION uses range from 0.0 to 1.0
         */
        progressBar.minValue = 0.0
        progressBar.maxValue = 1.0
    }

    func applicationWillTerminate(_ aNotification: Notification) { }

    /**
     Starts the work process. Demonstrates ex. data loading.
     This action is bound to the UI Start button.
     */
    @IBAction func doSomeWork(_ sender: Any) {
        startButton.isEnabled = false
        /**
         To do the work and keep UI responsive we put the work load in different thread.
         */
        DispatchQueue.global(qos: .userInitiated).async {
            self.workingClass.doWork(howMuch: 100000, status: self.progressBar)
        }
        startButton.isEnabled = true
    }
}

